<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
	public function index()
	{   
        if($this->session->userdata('logged_in')){
            redirect('/main#!/credit');    
        }
        else{
            $this->load->view('login');    
        }
		
	}
    
    public function checkCred(){
        $uname = $this->input->post('username');
        $pwd = $this->input->post('password');
        
        $this->load->model('loginmodel');
        $this->loginmodel->checkCred($uname,$pwd);
        
        redirect('/main#!/credit');
    }
    public function logout(){
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('/login');
    }
    
}
