<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    
	public function index()
	{
        if($this->session->userdata('logged_in')){
            $this->load->view('home');    
        }
        else{
            redirect('/login');
        }
		
	}
    
    //credit view controller
    public function credit()
	{
        if($this->session->userdata('logged_in')){
		    $this->load->view('partials/credit');
        }
        else{
            redirect('/login');
        }
	}
    
    //debit view controller
    public function debit()
	{
        if($this->session->userdata('logged_in')){
            $this->load->view('partials/debit');    
        }
        else{
            redirect('/login');
        }
		
	}
    
    //Add money to the account
    public function addMoney(){
    
        $money = array(
            'creditmoney'=>$this->input->post('creditmoney'),
            'creditmon'=>$this->input->post('creditmon'),
            'note'=>$this->input->post('note'),
            'timeadded'=>date('Y-m-d'),
        );
        $this->load->model('coremodel');
        $this->coremodel->addnew($money);
        redirect('/main#!/credit');
    }
    
    //Add expense to the account
    public function addExpense(){
        
        $ptype = $this->input->post('ptype');
        $cardMoney = array('cardebit' => $this->input->post('debitmoney'));
        
        $expense = array(
            'debitmoney'=>$this->input->post('debitmoney'),
            'debitpur'=>$this->input->post('debitpur'),
            'debitmon'=>$this->input->post('debitmon'),
            'method'=>$this->input->post('ptype'),
            'timeadded'=>date('Y-m-d'),
        );
        $this->load->model('coremodel');
        $this->coremodel->addexp($expense,$ptype,$cardMoney);
        redirect('/main#!/debit');
    }
    
    //call for credit table data
    public function showMoney()
	{   
        $this->load->model('coremodel');
        $data = $this->coremodel->fetchCreditInfo();
        $this->output->set_output($data);
	}
    
    //call for debit table data
    public function showExp()
	{   
        $this->load->model('coremodel');
        $data = $this->coremodel->fetchDebitInfo();
        $this->output->set_output($data);
	}
    
    //Shows available balance
    public function availableBal()
	{   
        $this->load->model('coremodel');
        $data = $this->coremodel->availableBal();
        $this->output->set_output($data);
	}
    
    //Shows amount spent from card
    public function cardExp()
	{   
        $this->load->model('coremodel');
        $data = $this->coremodel->cardExp();
        $this->output->set_output($data);
	}
    
}
