<?php

Class Coremodel extends CI_Model {
	
    Public function __construct() { 
        parent::__construct(); 
    }
    
    //returns credit table data
    function fetchCreditInfo() {
        $res = array();
        $this->db->select("*");
        $this->db->from("credit");
        
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $row){
            $res[] = $row;
        }
        
        if(empty($result)){    
            echo json_encode(0);
        }
        else{
            echo json_encode($res);
        }
    }
    
    //returns debit table data
    function fetchDebitInfo() {
        $res = array();
        $this->db->select("*");
        $this->db->from("debit");
        
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $row){
            $res[] = $row;
        }
        
        if(empty($result)){    
            echo json_encode(0);
        }
        else{
            echo json_encode($res);
        }
    }
    
    //Add money to the account
    public function addnew($money)
    {
        $res = array();
        $res2 = array();
        $this->db->insert('credit',$money);    
        
        $this->db->select("*");
        $this->db->from("avl_bal");
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $row){
            $res2[] = $row->availmoney;
        }
        
        if(empty($result)){    
            $this->db->select("*");
            $this->db->from("credit");
            $this->db->limit(1);
            $this->db->order_by('id',"DESC");
            $query = $this->db->get();
            $result = $query->result();
            
            foreach($result as $row){
                $res[] = $row->creditmoney;
            }
        
            $data = array('availmoney' => $res[0]);
            $this->db->insert('avl_bal',$data);    
        }
        else{
            $sum = $money['creditmoney'] + $res2[0];
            $data = array('availmoney' => $sum);
            $this->db->update('avl_bal',$data);    
        }

    }
    
    //Add expense to the account
    public function addexp($expense,$ptype,$cardMoney)
    {
        $res = array();
        $res2 = array();
        
        if($ptype == "card"){
            $this->db->insert('debit',$expense);    
            
            $this->db->select("*");
            $this->db->from("card_exp");
            $this->db->limit(1);
            $this->db->order_by('id',"DESC");
            $query = $this->db->get();
            $result = $query->result();
            
            foreach($result as $row){
                $res[] = $row->cardebit;
            }
            
            if(empty($result)){    
                $this->db->insert('card_exp',$cardMoney);        
            }
            else{
                $sum = $cardMoney['cardebit'] + $res[0];
                $data = array('cardebit' => $sum);
                $this->db->update('card_exp',$data);       
            }
        }
        else if($ptype == "cash"){
            $this->db->insert('debit',$expense);   
            
            $this->db->select("*");
            $this->db->from("avl_bal");
            $this->db->limit(1);
            $this->db->order_by('id',"DESC");
            $query = $this->db->get();
            $result = $query->result();

            foreach($result as $row){
                $res2[] = $row->availmoney;
            }
            $diff = $res2[0] - $expense['debitmoney'];
            $data = array('availmoney' => $diff);
            $this->db->update('avl_bal',$data);     
        }
    }
    
    //Shows available balance
    public function availableBal()
    {
        $res = array();
        $this->db->select("*");
        $this->db->from("avl_bal");
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $row){
            $res[] = $row->availmoney;
        }
        
        if(empty($result)){    
            echo json_encode(0);
        }
        else{
            echo $res[0];
        }

    }

    //Shows amount spent from card
    public function cardExp()
    {
        $res = array();
        $this->db->select("*");
        $this->db->from("card_exp");
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $row){
            $res[] = $row->cardebit;
        }
        
        if(empty($result)){    
            echo json_encode(0);
        }
        else{
            echo $res[0];
        }
    }

}

?>