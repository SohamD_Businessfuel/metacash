<?php

Class loginmodel extends CI_Model {
	
    Public function __construct() { 
        parent::__construct(); 
    }

    function checkCred($uname,$pwd) {
        
        $this->load->database();    
        
        $res = $this->db->select('username,password');
        $res = $this->db->from('userinfo');
        $res = $this->db->where('username',$uname);
        $res = $this->db->where('password',$pwd);
        
        $query = $this->db->get();
        $res = $query->result();
    
        if($res){
            $sess_array = array('username' => $uname, 'password' => $pwd);
            $this->session->set_userdata('logged_in',$sess_array);
        }
        else{
            redirect('/login');
        }
        
        
    }

}

?>