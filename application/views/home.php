<html ng-app="metaApp">
<head>
    <title>MetaCash - Never lose your money</title>
    <?php include'head.php' ?>
</head>
<body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top navbar-mod">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" ui-sref="home" id="home">MetaCash</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav"></ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Settings</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo base_url().'/login/logout/';?>">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 sidebar sidebar-mod">
          <a class="navbar-brand navbar-brand-mod" href="#">MetaCash</a>
          <ul class="nav nav-sidebar">
            <li><a ui-sref="credit" id="credit">Credit</a></li>
            <li><a ui-sref="debit" id="debit">Debit</a></li>
            <li><a ui-sref="#" id="analytics">Analytics</a></li>
            <li><a ui-sref="#" id="export">Export</a></li>
          </ul>
        </div>
        <div class="col-md-offset-2 col-md-10">
            <div class="content">
                <div class="col-md-5">
                    <h3 class="available-bal" ng-controller="availableBal">Available Balance : <span>Rs. {{availableBal}}</span></h3>
                </div>
                <div class="col-md-5">
                    <h3 class="available-bal" ng-controller="cardExp">Card Expenses : <span>Rs. {{cardExp}} </span></h3>
                </div>
                <ui-view></ui-view>  
            </div>
        </div>
      </div>
    </div>
        
    
</body>
</html>