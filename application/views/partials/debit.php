<div class="container">
    <div class="row">
        <div class="col-md-9" ng-controller="debitForm">
            <form action="<?php echo base_url().'main/addExpense'; ?>" method="POST">
                <div class="col-md-2">
                  <div class="form-group">
                    <div class="input-group">
                        <div id="radioBtn" class="btn-group">
                            <a class="btn btn-success btn-sm active" data-toggle="ptype" data-title="cash">Cash</a>
                            <a class="btn btn-primary btn-sm notActive" data-toggle="ptype" data-title="card">Card</a>
                        </div>
                        <input type="hidden" name="ptype" id="ptype" value="cash">
                    </div>
    	           </div>
                </div>
                <div class="col-md-3">
                    <input type="number" class="form-control" placeholder="Enter amount" name="debitmoney" required/>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" placeholder="Enter purpose" name="debitpur" required/>
                </div>
                <div class="col-md-3">
                    <select class="form-control" name="debitmon" required>
                        <option selected="selected" disabled="disabled" value="">Select Month</option>
                        <option value="Jan">Jan</option>
                        <option value="Feb">Feb</option>
                        <option value="Mar">Mar</option>
                        <option value="Apr">Apr</option>
                        <option value="May">May</option>
                        <option value="Jun">June</option>
                        <option value="Jul">July</option>
                        <option value="Aug">Aug</option>
                        <option value="Sep">Sep</option>
                        <option value="Oct">Oct</option>
                        <option value="Nov">Nov</option>
                        <option value="Dec">Dec</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-success">Add Expense</button>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-11" ng-controller="debitCtrl">
            <h3>Debit Summary</h3>
            <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Added On</th>
                <th>Purpose</th>
                <th>For Month</th>
                <th>Payment Method</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="x in debinfo | orderBy:'_id':true">
                <td>{{x.timeadded}}</td>
                <td> {{x.debitpur}}</td>
                <td> {{x.debitmon}}</td>
                <td> {{x.method}}</td>
                <td>Rs. {{x.debitmoney}}</td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
    
</div>