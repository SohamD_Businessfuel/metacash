<div class="container">
    <div class="row">
        <div class="col-md-9">
            <form action="<?php echo base_url().'main/addMoney'; ?>" method="POST">
                <div class="col-md-3">
                    <input type="number" class="form-control" placeholder="Enter amount" name="creditmoney" required/>
                </div>
                <div class="col-md-3">
                    <select class="form-control" name="creditmon" required>
                        <option selected="selected" disabled="disabled" value="">Select Month</option>
                        <option value="Jan">Jan</option>
                        <option value="Feb">Feb</option>
                        <option value="Mar">Mar</option>
                        <option value="Apr">Apr</option>
                        <option value="May">May</option>
                        <option value="Jun">June</option>
                        <option value="Jul">July</option>
                        <option value="Aug">Aug</option>
                        <option value="Sep">Sep</option>
                        <option value="Oct">Oct</option>
                        <option value="Nov">Nov</option>
                        <option value="Dec">Dec</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" placeholder="Write note" name="note" required/>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-success">Add Money</button>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-11" ng-controller="creditCtrl">
            <h3>Credit Summary</h3>
            <table class="table table-striped">
            <thead>
              <tr>
                <th>Amount</th>
                <th>For Month</th>
                <th>Notes</th>
                <th>Added On</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="x in credinfo | orderBy:'_id':true">
                <td>Rs. {{x.creditmoney}}</td>
                <td> {{x.creditmon}}</td>
                <td> {{x.note}}</td>
                <td>{{x.timeadded}}</td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
    
</div>