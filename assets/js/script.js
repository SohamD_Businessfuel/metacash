//var base_url = "http://localhost/";
var base_url = "http://stage.metacrust.com/";
var metaApp = angular.module('metaApp', ['ui.router']);

metaApp.config(function($stateProvider, $urlRouterProvider) {
    
    //$urlRouterProvider.otherwise('/home');
    var credit = {
        name:'credit',
        url: '/credit',
        templateUrl: 'main/credit'
    }
    var debit = {
        name:'debit',
        url: '/debit',
        templateUrl: 'main/debit'
    }
    var home = {
        name:'home',
        url: '/main',
        templateUrl: '/home'
    }
    $stateProvider
        .state(credit)
        .state(debit)
        .state(home);
});

//credit controller
metaApp.controller('creditCtrl',function($scope,$http){
    $("#credit").addClass("active-mod");
    $("#debit").removeClass("active-mod");
    $("#analytics").removeClass("active-mod");
    $("#export").removeClass("active-mod");
    $http.get(base_url+"metacash/main/showMoney/")
        .then(function(response) {
            $scope.credinfo = response.data;
            console.log(response.data);
        }); 
});

//debit controller
metaApp.controller('debitCtrl',function($scope,$http){
    $("#credit").removeClass("active-mod");
    $("#debit").addClass("active-mod");
    $("#analytics").removeClass("active-mod");
    $("#export").removeClass("active-mod");
    $http.get(base_url+"metacash/main/showExp/")
        .then(function(response) {
            $scope.debinfo = response.data;
            console.log(response.data);
        }); 
});

//available balance controller
metaApp.controller('availableBal',function($scope,$http){
    $http.get(base_url+"metacash/main/availableBal/")
        .then(function(response) {
            $scope.availableBal = response.data;
            console.log(response.data);
        }); 
});

//card expenses controller
metaApp.controller('cardExp',function($scope,$http){
    $http.get(base_url+"metacash/main/cardExp/")
        .then(function(response) {
            $scope.cardExp = response.data;
            console.log(response.data);
        }); 
});

//debit method controller
metaApp.controller('debitForm',function($scope){
    $(document).ready(function(){
        $('#radioBtn a').on('click', function(){
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            $('#'+tog).prop('value', sel);

            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
        });    
    });
});

